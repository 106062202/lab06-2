const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.makeQQNode = functions.database.ref('/com_list/{pushId}')
  .onCreate((snapshot, context) => {
    const original = snapshot.val();

    const msg = original.post + 'QQ';

    return snapshot.ref.child('addedQQData').set(msg);
  });
